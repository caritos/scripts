#!/bin/bash
echo "converting ebooks to pdf...";
mv */* .
rmdir *
shopt -s nullglob
for file in ./*.epub; do ebook-convert "$file" "${file%.*}.pdf"; done
shopt -s nullglob
for file in ./*.azw3; do ebook-convert "$file" "${file%:*}.pdf"; done
shopt -s nullglob
for file in ./*.mobi; do ebook-convert "$file" "${file%:*}.pdf"; done
rm -f ./FoxEbook.net.txt
rm -f ./*.txt
rm -f ./*.mobi
rm -f ./*.epub
rm -f ./*.azw3
mv ./*.pdf /Users/eladio/Documents/programming-books/
